<?php
/**
 * CLICKATELL SMS API
 *
 * This class is meant to send SMS messages via the Clickatell gateway
 * and provides support to authenticate to this service and also query
 * for the current account balance. This class use the fopen or CURL module
 * to communicate with the gateway via HTTP/S.
 *
 * For more information about CLICKATELL service visit http://www.clickatell.com
 *
 * version 1.3d
 * package gateway_clickatell_http
 * @author Aleksandar Markovic <mikikg@gmail.com>, David Hamilton
 * @copyright Copyright � 2004, 2005 Aleksandar Markovic
 * @link http://sourceforge.net/projects/sms-api/ SMS-API Sourceforge project page
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 */

/** returns gateway name
 * @return the human-readable name of the provider plugin (service provider and protocol), for use in selection user interface
 */
function smsgateway_gateway_clickatell_http_name() {
  return t('Clickatell HTTP/S');
}

/** returns gateway description text
 * @return The human-readable decription of the module, containing more information about where to go to get information about
 *         the service provider/protocol. May contain HTML formatting.
 */
function smsgateway_gateway_clickatell_http_description() {
  return t('Clickatell HTTP/S Gateway. For details and account information visit <a href=\'http://www.clickatell.com/\'>http://www.clickatell.com/</a>');
}

/** sends a single message to the gateway provider.
 * @param smssettings array of settings containing the account information to allow message transmission. Fields are
                      'serverurl' The URL used to connect to the server.
                      'account_id' The identifier of the account to use
                      'accountname' The account or username used to login
                      'password' The password to use to login
                      'from' The telephone number to be used for the sender
                      'usessl' 1 to indicate the use of an SSL connection
   @param message array of message specific variables
                      'destination_number' the destination telephone number to send the message to
                      'message_body' the text body of the message
   @return an array of response variables.
                      'response' the vendor specific text response as sent by the provider gateway server
                      'connectstring' a debug string indicating the string used to connect to the server (if supported)
                      'initialbalance' the balance at the start of the send (if supported)
*/
function smsgateway_gateway_clickatell_http_sendsingle( $smssettings, $message) {
  $destination_number = trim($message['destination_number']);
  $message_body = trim($message['message_body']);
  $result = array();
  $smsapi = new clickatell($smssettings['account_id'], $smssettings['accountname'], $smssettings['password'], $smssettings['usessl']);
  $result['connectstring'] = $smsapi->build_connection_string();
  $result['initialbalance'] = $smsapi->getbalance();
  $result['response'] = $smsapi->send($destination_number, $smssettings['from'], $message_body);
  return $result;
}

/** sends a batch of messages to the gateway provider.
 * @param smssettings array of settings containing the account information to allow message transmission. Fields are
                      'serverurl' The URL used to connect to the server.
                      'account_id' The identifier of the account to use
                      'accountname' The account or username used to login
                      'password' The password to use to login
                      'from' The telephone number to be used for the sender
                      'usessl' 1 to indicate the use of an SSL connection
   @param sms_messages an array of messages keyed by a unique ID for the message. This is used to track the responses for the
              individual messages. Each individual message is itself an array of message specific variables:
                      'destination_number' the destination telephone number to send the message to
                      'message_body' the text body of the message
   @return an array of response variables.
                      'connectstring' a debug string indicating the string used to connect to the server (if supported)
                      'initialbalance' the balance at the start of the send (if supported)
                      Id-specific - the Unique ID passed in with each message is used to track the vendor specific text response as
                          sent by the provider gateway server
*/
function smsgateway_gateway_clickatell_http_sendbatch($smssettings, $sms_messages) {
  $smsapi = new clickatell($smssettings['account_id'], $smssettings['accountname'], $smssettings['password'], $smssettings['usessl']);
  $result['connectstring'] = $smsapi->build_connection_string();
  $result['initialbalance'] = $smsapi->getbalance();
  foreach( $sms_messages as $msgid => $message) {
    $destination_number = trim($message['destination_number']);
    $message_body = trim($message['message_body']);
    if ($destination_number == null || strlen($destination_number) == 0) {
      $result[ $msgid] = 'No telephone number specified';
      }
    else {
      $result[ $msgid] = $smsapi->send( $destination_number, $smssettings['from'], $message_body);
      }
    }
  return $result;
}

/** parses a received message. Ensures that all received parameters are HTML displayable.
 * @return the values from the message parsed into an array of variables keyed as follows:
                      'account_id' the ID of the account that the message was received on
                      'sender_number' the telephone number of the message sender
                      'destination_number' the telephone number that the message was sent to
                      'timestamp' the timestamp placed on the message
                      'charset' optional character set information for the message body
                      'header' any additional header information
                      'body' the body of the message - the message text
*/
function smsgateway_gateway_clickatell_http_parsereceived() {
  $result = array();
  $result['account_id'] = check_plain($_GET['api_id']);
  $result['sender_number'] = check_plain($_GET['from']);
  $result['destination_number'] = check_plain($_GET['to']);
  $result['timestamp'] = check_plain($_GET['timestamp']);
  $result['charset'] = check_plain($_GET['charset']);
  $result['header'] = check_plain($_GET['udh']);
  $result['body'] = check_plain($_GET['text']);

  return $result;
}

/**
 * Main SMS-API class
 *
 * Example:
 * <code>
 * <?php
 * require_once ("sms_api.php");
 * $mysms = new sms();
 * echo $mysms->session;
 * echo $mysms->getbalance();
 * $mysms->send ("38160123", "netsector", "TEST MESSAGE");
 * ?>
 * </code>
 * @package clickatell_api
 */

class clickatell {

    /**
    * Clickatell API-ID
    * @link http://sourceforge.net/forum/forum.php?thread_id=1005106&forum_id=344522 How to get CLICKATELL API ID?
    * @var integer
    */
    var $api_id = "test";

    /**
    * Clickatell username
    * @var mixed
    */
    var $user = "test";

    /**
    * Clickatell password
    * @var mixed
    */
    var $password = "test";

    /**
    * Use SSL (HTTPS) protocol
    * @var bool
    */
    var $use_ssl = false;

    /**
    * Define SMS balance limit below class will not work
    * @var integer
    */
    var $balace_limit = 0;

    /**
    * Gateway command sending method (curl,fopen)
    * @var mixed
    */
    var $sending_method = "fopen";

    /**
    * Optional CURL Proxy
    * @var bool
    */
    var $curl_use_proxy = false;

    /**
    * Proxy URL and PORT
    * @var mixed
    */
    var $curl_proxy = "http://127.0.0.1:8080";

    /**
    * Proxy username and password
    * @var mixed
    */
    var $curl_proxyuserpwd = "login:secretpass";

    /**
    * Callback
    * 0 - Off
    * 1 - Returns only intermediate statuses
    * 2 - Returns only final statuses
    * 3 - Returns both intermediate and final statuses
    * @var integer
    */
    var $callback = 0;

    /**
    * Session variable
    * @var mixed
    */
    var $session;

    /**
    * Class constructor
    * Create SMS object and authenticate SMS gateway
   * @param new_api_id API ID value
    * @param new_user user/account name
    * @param new_password password to use
    * @param new_use_ssl whether to use SSL
    * @return object New SMS object.
    * @access public
    */
    function clickatell ($new_api_id, $new_user, $new_password, $new_use_ssl = false) {
        $this->api_id = $new_api_id;
        $this->user = $new_user;
        $this->password = $new_password;
	$this->use_ssl = $new_use_ssl;
        if ($this->use_ssl) {
            $this->base   = "http://api.clickatell.com/http";
            $this->base_s = "https://api.clickatell.com/http";
        }
        else {
            $this->base   = "http://api.clickatell.com/http";
            $this->base_s = $this->base;
        }

        $this->_auth();
    }

   /**
    * Return gateway connection string
    * @return The gateway connection string
    * @access public
    */
    function build_connection_string() {
    	return sprintf ("%s/auth?api_id=%s&user=%s&password=%s", $this->base_s, $this->api_id, $this->user, $this->password);
    }


  /**
    * Set sending vars

    * @return mixed  "OK" or exception describing error
    * @access private
    */
    function _auth() {
        $this->session = $this->_parse_auth ($this->_execgw( $this->build_connection_string()));
    }

    /**
    * Query SMS credis balance
    * @return integer  number of SMS credits
    * @access public
    */
    function getbalance() {
    	$comm = sprintf ("%s/getbalance?session_id=%s", $this->base, $this->session);
        return $this->_parse_getbalance ($this->_execgw($comm));
    }

    /**
    * Send SMS message
    * @param to mixed  The destination address.
    * @param from mixed  The source/sender address
    * @param text mixed  The text content of the message
    * @return mixed  "OK" or exception describing error
    * @access public
    */
    function send($to=null, $from=null, $text=null) {

    	/* Check SMS credits balance */
    	if ($this->getbalance() < $this->balace_limit) {
    	    return ("Failed: Insufficient Credit with gateway.");
    	};

    	/* Check SMS $text length */
        if (drupal_strlen ($text) > 465) {
    	    return ("Failed: The message is to long, current length=".drupal_strlen ($text)." chars.");
    	}

    	/* Does message need to be concatenate */
        if (drupal_strlen ($text) > 160) {
            $concat = "&concat=3";
    	}
      else {
            $concat = "";
        }

    	/* Check $to and $from is not empty */
        if (empty ($to)) {
    	    return ("Failed: No destination address specified.");
    	}
        if (empty ($from)) {
    	    return ("Failed: No from address specified.");
    	}

    	/* Reformat $to number */
        $cleanup_chr = array ("+", " ", "(", ")", "\r", "\n", "\r\n");
        $to = str_replace($cleanup_chr, "", $to);

    	/* Send SMS now */
    	$comm = sprintf ("%s/sendmsg?session_id=%s&to=%s&from=%s&text=%s&callback=%s%s",
            $this->base,
            $this->session,
            rawurlencode($to),
            rawurlencode($from),
            rawurlencode($text),
            $this->callback,
            $concat
        );
        return $this->_parse_send ($this->_execgw($comm));
    }

    /**
    * Execute gateway commands
    * @access private
    */
    function _execgw($command) {
        if ($this->sending_method == "curl")
            return $this->_curl($command);
        if ($this->sending_method == "fopen")
            return $this->_fopen($command);
        return ("Failed: Unsupported sending method selected");
    }

    /**
    * CURL sending method
    * @access private
    */
    function _curl($command) {
        $this->_chk_curl();
        $ch = curl_init ($command);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER,0);
        if ($this->curl_use_proxy) {
            curl_setopt ($ch, CURLOPT_PROXY, $this->curl_proxy);
            curl_setopt ($ch, CURLOPT_PROXYUSERPWD, $this->curl_proxyuserpwd);
        }
        $result=curl_exec ($ch);
        curl_close ($ch);
        return $result;
    }

    /**
    * fopen sending method
    * @access private
    */
    function _fopen($command) {
        $result = '';
        $handler = @fopen ($command, 'r');
        if ($handler) {
            while ($line = @fgets($handler,1024)) {
                $result .= $line;
            }
            fclose ($handler);
            return $result;
        }
        else {
            return ("Failed: Unable to open remote connection. Check that PHP version greater than 4.3.0 and OpenSSL support installed.");
        }
    }

    /**
    * Parse authentication command response text
    * @access private
    */
    function _parse_auth ($result) {
    	$session = drupal_substr($result, 4);
        $code = drupal_substr($result, 0, 2);
        if ($code!="OK") {
            return ("Failed: Unable to get gateway authorization! Code: ".$result);
        }
        return $session;
    }

    /**
    * Parse send command response text
    * @access private
    */
    function _parse_send ($result) {
    	$code = drupal_substr($result, 0, 2);
    	if ($code!="ID") {
    	    return ("Failed: SMS Gateway returned error: Code: ".$result);
    	}
      else {
    	    return "OK: ".$result;
    	}
        return $result;
    }

    /**
    * Parse getbalance command response text
    * @access private
    */
    function _parse_getbalance ($result) {
    	$result = drupal_substr($result, 8);
        return (int)$result;
    }

    /**
    * Check for CURL PHP module
    * @access private
    */
    function _chk_curl() {
        if (!extension_loaded('curl')) {
            return ("Failed: PHP CURL module not installed.");
        }
    }
}


?>