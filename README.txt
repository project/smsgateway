
SMSGateway
----------

This is a module that allows the sending/receiving of SMS messages from/to a 
Drupal website. You need an account with a SMS Service Provider to make this 
work.

Currently the system only provides support for the Clickatell Service Provider
and only for HTTP/HTTPS protocol message sending. See Gateway Services below for 
more details.

This module only has configuration, logging and self-test functionality. In 
order to use it as part of your site, you need to adapt your own modules.

To send a message, call the 'smsgateway_sendmessage' function, with the 
destination telephone number and the message body text as parameters.
The function returns an array with the response from the Service Provider
Gateway in the 'response' key, the string used to contact the gateway in the 
'connectstring' key (used for debugging) and the balance at the start of the
transmission under the 'initialbalance' key. 

To send more than one message at a time, call the 'smsgateway_sendmessageset' 
function, with an array of messages keyed by unique ID (used for tracking the 
responses) and subarray with the destination number in 'destination_number' and
the text of the message in 'message_body'.
The function returns an array with the string used to contact the gateway in the 
'connectstring' key (used for debugging), the balance at the start of the
transmission under the 'initialbalance' key and the response from the Service 
Provider Gateway for each message under the uid key provided.

The message reception requires requires 'anonymous user' access to be set for
the 'receive sms message' permission. Modules that wish to be informed of a 
newly arrived message should implement the 'smsgateway_newmessage' hook and
process the array passed as a parameter with the message parts in the following 
fields:
        'account_id' the ID of the account that the message was received on
        'sender_number' the telephone number of the message sender
        'destination_number' the telephone number that the message was sent to
        'timestamp' the timestamp placed on the message
        'charset' optional character set information for the message body
        'header' any additional header information
        'body' the body of the message - the message text


Gateway Services
----------------

Clickatell - http://www.clickatell.com/

This provides a good value range of services for low-medium volumes of messages.
Registration is straightforward - for the HTTP/S service go to:
http://www.clickatell.com/central/login.php?prod_id=2
and fill out the form. Currently they give 10 free credits on registration.

To Do
-----

Add support for the Clickatell bulk message sending to improve the speed of 
sending multiple messages. Currently the smsgateway_sendmessageset method logs 
in once, but sends the actual messages using the single message-send protocol.

Add filtering support for SMS logs and support for autodeletion after a defined 
period.

Add support for alternative Service Providers.

Add support for other protocols, notably the XML method supported by 
Clickatell.


Special Thanks
--------------

Aleksandar Markovic for providing the Clickatell Service Provider API.
http://sourceforge.net/projects/sms-api/

iMustard.com - http://www.imustard.com - for sponsoring this module.

--
David Hamilton <david dot hamilton at jiffle dot net>