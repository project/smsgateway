
SMS Service Provider Interface
==============================

Introduction
------------
This document defines the interface between the main SMS Gateway module and the 
Service Provider plugins. There are 5 hook methods for the plugin to implement.
In all these example 'provider' is used in place of the name of the plugin 
module. 

Module Naming and Discovery
---------------------------
The provider plugin module should be named 'gateway_provider.inc' where 
'provider' is replaced by the name of the plugin as described above. This 
normally would be a combination of the company name of the gateway service 
provider followed by an underscore and the protocol used.

For example 'clickatell_http' for the HTTP Protocol implementation of the 
Clickatell Service API.

Plugin Name
-----------
This hook method is used to retrieve the human-readable name of the provider 
plugin, for use in selection user interface.

Hook Name: smsgateway_gateway_provider_name
Parameters: None.
Returns: String containing the human-readable name of service provider and 
  protocol.
  
Plugin Description
------------------
This hook method is used to retrieve the human-readable decription of the 
module, containing more information about where to go to get information about
the service provider and protocol, allowing a user to register an account with
the service provider.
 
Send Single Message
-------------------
This hook method sends a single message to the gateway provider.

Hook Name: smsgateway_gateway_provider_sendsingle
Parameters: 
  (1) smssettings - array of settings containing the account information to 
      allow message transmission. 
        'serverurl' The URL used to connect to the server.
        'account_id' The identifier of the account to use
        'accountname' The account or username used to login
        'password' The password to use to login
        'from' The telephone number to be used for the sender
        'usessl' 1 to indicate the use of an SSL connection
  (2) message - array of message specific variables
        'destination_number' the destination telephone number to send the 
	    message to
        'message_body' the text body of the message
Returns: an array of response variables.
        'response' the vendor specific text response as sent by the provider 
	    gateway server
        'connectstring' a debug string indicating the string used to connect to 
	    the server (if supported) 
        'initialbalance' the balance at the start of the send (if supported)

Send Single Message
-------------------
This hook method sends a batch of messages to the gateway provider.

Hook Name: smsgateway_gateway_provider_sendbatch
Parameters: 
  (1) smssettings - array of settings containing the account information to 
      allow message transmission. 
        'serverurl' The URL used to connect to the server.
        'account_id' The identifier of the account to use
        'accountname' The account or username used to login
        'password' The password to use to login
        'from' The telephone number to be used for the sender
        'usessl' 1 to indicate the use of an SSL connection
  (2) message - an array of messages keyed by a unique ID for the message. This 
      is used to track the responses for the individual messages. Each 
      individual message is itself an array of message specific variables:
        'destination_number' the destination telephone number to send the 
	    message to
        'message_body' the text body of the message
Returns: an array of response variables.
        'connectstring' a debug string indicating the string used to connect to 
	    the server (if supported) 
        'initialbalance' the balance at the start of the send (if supported)
        Id-specific - the Unique ID passed in with each message is used to track 
	    the vendor specific text response as sent by the provider gateway 
	    server

Parse Received Message
----------------------
This hook message parses a received message into a keyed array of variables. 
Currently the information is either from the URL get or post variables.
This function should ensure that all returned strings are HTML displayable 
by calling check_plain() on them.

Hook Name: smsgateway_gateway_provider_sendbatch
Parameters: None 
Returns: The received message as array of response variables keyed as follows:.
        'account_id' the ID of the account that the message was received on
        'sender_number' the telephone number of the message sender
        'destination_number' the telephone number that the message was sent to
        'timestamp' the timestamp placed on the message
        'charset' optional character set information for the message body
        'header' any additional header information
        'body' the body of the message - the message text

